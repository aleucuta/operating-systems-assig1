#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>

#define MAX_PATH 255
#define MAX_ARGUMENT_SIZE 1000
int err;

typedef struct{
    char sect_name[14];
    unsigned short sect_type;
    unsigned int sect_offset;
    unsigned int sect_size;
}SECTION_DATA;

typedef struct{
    char magic;
    unsigned short header_size;
    unsigned int version;
    unsigned char no_of_sections;
    SECTION_DATA *sections;
}FILE_DATA;

int variantPrint()
{
    printf("74123\n");
    return 0;
}

int errHandle(int err);

int listDir(const char path[MAX_PATH],char flags,int size_greater,int permissions)
{
    DIR *dir = NULL;
    struct dirent *entry = NULL;
    dir = opendir(path);
    char big_path[512];
    struct stat stat_buffer;
    char print_ok = 0;

    if(dir==NULL){
        //err = 1;
        return 0;
    }
    while((entry = readdir(dir))!=NULL){
        if(strcmp(entry->d_name,".")!=0 && strcmp(entry->d_name,"..")!=0)
        {   print_ok = 1;
            snprintf(big_path,512,"%s/%s",path,entry->d_name);
            if(lstat(big_path,&stat_buffer) == 0)
            {   
                //if(recc = 0)
                
                //printf("%s--%o %o\n",entry->d_name,stat_buffer.st_mode ,permissions);
                if((flags & 4) && ((stat_buffer.st_mode & 0777) != permissions))
                {
                    print_ok = 0;
                }
                if((flags & 2) && ((stat_buffer.st_size <= size_greater)))
                {
                    print_ok = 0;
                }
                if(print_ok){
                    printf("%s\n",big_path);
                    //printf("%o - %o\n",stat_buffer.st_mode,permissions);
                }
                if(S_ISDIR(stat_buffer.st_mode))
                {   
                    if(flags & 1)
                        listDir(big_path,flags,size_greater,permissions);
                }
                
                
                else
                {
                   // if(recc = 1)
                     // printf("%s/%s\n",path,entry->d_name);
                }
            }
        }
        
    }
    closedir(dir);
    //printf("List");
    return 0;
}

int parseFile(int file_handle,int id)
{
    int err = 0;
    FILE_DATA file;
    char *file_buffer;
    while(1)
    {   
        if(file_handle == -1){
            err = 3;
            break;
        }
        char magic;
        read(file_handle,&magic,1);
        if(magic != 'F')
        {   
            err = 4;
            break;
        }
        file.header_size = 0;
        read(file_handle,&(file.header_size),2);
        
        file_buffer = (char*)malloc(file.header_size * sizeof(char) - 2);
        if(file_buffer == NULL)
        {
            err = 1;
            break;
        }
        file_buffer[file.header_size-1] = '\0'; 
        
        read(file_handle,file_buffer, file.header_size);

        unsigned int offset = 0;
        file.version = 0;
        sscanf(file_buffer, "%4c", (char*)(&(file.version)));
        if(file.version < 28 || file.version > 117)
        {
            err = 5;
            free(file_buffer);
            break;
        }
        offset+=4;

        file.no_of_sections = 0;
        sscanf(file_buffer+offset,"%c", &(file.no_of_sections));
        if(file.no_of_sections < 6 || file.no_of_sections > 12)
        {
            err = 6;
            free(file_buffer);
            break;
        }
        offset+=1;

        file.sections = (SECTION_DATA*)malloc(file.no_of_sections * sizeof(SECTION_DATA));
        for(int i=0;i<file.no_of_sections;i++)
        {
            memcpy(file.sections[i].sect_name,file_buffer+offset,13);
            file.sections[i].sect_name[13] = '\0';
            file.sections[i].sect_type=0;
            file.sections[i].sect_offset=0;
            file.sections[i].sect_size=0;
            offset+=13;
            memcpy(&(file.sections[i].sect_type),file_buffer+offset,2);
            offset+=2;
            if(file.sections[i].sect_type != 34 &&
                file.sections[i].sect_type != 38 &&
                file.sections[i].sect_type != 52 &&
                file.sections[i].sect_type != 31 &&
                file.sections[i].sect_type != 35)
                {
                    err = 7;
                    free(file_buffer);
                    free(file.sections);
                    break;
                }
            memcpy(&(file.sections[i].sect_offset),file_buffer+offset,4);
            offset+=4;
            memcpy(&(file.sections[i].sect_size),file_buffer+offset,4);
            offset+=4;
            
        }

        break;
    } 
    if(err == 0){
        printf("SUCCESS\n");
        printf("version=%d\n",file.version);
        printf("nr_sections=%d\n",file.no_of_sections);
        for(int i=0;i<file.no_of_sections;i++)
        {
            printf("section%d: %s %d %d\n",i+1,file.sections[i].sect_name,
                                             file.sections[i].sect_type,
                                             file.sections[i].sect_size);
        }

        free(file_buffer);
        free(file.sections);
        
    }
    //free(file_buffer);
    //free(file.sections);
    return err;
}

int extractLine(int file_handle,int sect_nr,int line_nr)
{
    //FILE_DATA file;
    //printf("%d %d %d",file_handle,sect_nr,line_nr);
    //char *file_buffer;
    int err = 0;
    while(1)
    {   
        
        if(file_handle == -1){
            err = 8;
            break;
        }
        lseek(file_handle,7,SEEK_SET);
        char nr_sections;
        read(file_handle,&nr_sections,1);
        if(nr_sections < sect_nr)
        {
            err = 9;
            break;
        }
        lseek(file_handle,23*(sect_nr-1)+15,SEEK_CUR);
        unsigned int sect_offset=0,sect_size = 0;
        read(file_handle,&sect_offset,4);
        read(file_handle,&sect_size,4);
       // printf("\noff: %x\nsize: %x\n",sect_offset,sect_size);
        char buffer[sect_size+1];
        int count = 0;
        int index = 0;
        lseek(file_handle,sect_offset+sect_size-1,SEEK_SET);
        //printf("%x",sect_offset+sect_size);
        for(int i=sect_offset+sect_size;i>=sect_offset-1;i--)
        {
            char c;
            read(file_handle,&c,1);
            if(c==0x0A || c==0x00)
            {   
                count++;
                //printf("SPACE");
            }
            if(count==line_nr){
                read(file_handle,buffer,sect_size);
                while(buffer[index]!=0x0A && buffer[index]!=0x00){
                    index++;
                }
                break;
            }
            lseek(file_handle,-2,SEEK_CUR);
            
        }
        //printf("ind %d",index);
        if(count < line_nr && index==0)
        {
            err = 10;
            break;
        }
        else{
            printf("SUCCESS\n");
            buffer[index]='\0';
            //strrev(buffer);
            for(int i=1;i<=index;i++){
                printf("%c",buffer[index-i]);
            }
            //printf("%s",buffer);
        }
        break;
    }
    return err;
}


int findAllParse(const char path[MAX_PATH])
{
    int err = 0;
    FILE_DATA file;
    char *file_buffer;
    int file_handle = -1;
    printf("xx.%s",path);
            
    file_handle = open(path,O_RDONLY);
    while(1)
    {   
        if(file_handle == -1){
            err = 1;
            break;
        }
        char magic;
        read(file_handle,&magic,1);
        if(magic != 'F')
        {   
            err = 1;
            break;
        }
        file.header_size = 0;
        read(file_handle,&(file.header_size),2);
        
        file_buffer = (char*)malloc(file.header_size * sizeof(char) - 2);
        if(file_buffer == NULL)
        {
            printf("xx.%s",path);
            err = 1;
            break;
        }
        file_buffer[file.header_size-1] = '\0'; 
        
        read(file_handle,file_buffer, file.header_size);

        unsigned int offset = 0;
        file.version = 0;
        sscanf(file_buffer, "%4c", (char*)(&(file.version)));
        if(file.version < 28 || file.version > 117)
        {
            err = 1;
            free(file_buffer);
            break;
        }
        offset+=4;

        file.no_of_sections = 0;
        sscanf(file_buffer+offset,"%c", &(file.no_of_sections));
        if(file.no_of_sections < 6 || file.no_of_sections > 12)
        {
            err = 1;
            free(file_buffer);
            break;
        }
        offset+=1;

        file.sections = (SECTION_DATA*)malloc(file.no_of_sections * sizeof(SECTION_DATA));
        for(int i=0;i<file.no_of_sections;i++)
        {
            memcpy(file.sections[i].sect_name,file_buffer+offset,13);
            file.sections[i].sect_name[13] = '\0';
            file.sections[i].sect_type=0;
            file.sections[i].sect_offset=0;
            file.sections[i].sect_size=0;
            offset+=13;
            memcpy(&(file.sections[i].sect_type),file_buffer+offset,2);
            offset+=2;
            if(file.sections[i].sect_type != 34 &&
                file.sections[i].sect_type != 38 &&
                file.sections[i].sect_type != 52 &&
                file.sections[i].sect_type != 31 &&
                file.sections[i].sect_type != 35)
                {
                    err = 1;
                    free(file_buffer);
                    free(file.sections);
                    break;
                }
            memcpy(&(file.sections[i].sect_offset),file_buffer+offset,4);
            offset+=4;
            memcpy(&(file.sections[i].sect_size),file_buffer+offset,4);
            offset+=4;
        }

        break;
    } 
    int check = 0;
    for(int i=0;i<file.no_of_sections;i++)
    {
        lseek(file_handle,file.sections[i].sect_offset,SEEK_SET);
        char buffer[1000];
        int count =0;
        int bf_len = 0;
        int size = file.sections[i].sect_size;
        //printf("%d,%d\n",file.sections[i].sect_offset,file.sections[i].sect_size);
        while((bf_len = read(file_handle,buffer,999)))
        {
            
            //printf("\n%d\n",bf_len);
            for(int i=0;i<bf_len && i<size;i++)
            {   
                //printf("%d\n",i);
                if(buffer[i] == 0x0A)
                {
                    count++;
                }
            }
            size-=bf_len;
            
            if(size<0)
                break;
            //if(count == 13)
              //  break;
        }
        //printf("%d : %d\n",i,count);
        if(count == 13)
        {
            check = 1;
            break;
        }
    }   
    free(file_buffer);
    free(file.sections);

    // if(err == 0 && check == 1){
    //     printf("SUCCESS\n");
    //     printf("%s",path);
           
    // }
    //printf("%d, %d\n",err,check);
    errHandle(err);
    return check;
}

int rec_error = 0;
int success_printed = 0;

int findAll(const char path[MAX_PATH])
{
    DIR *dir = NULL;
    struct dirent *entry = NULL;
    char fullPath[512];
    struct stat statbuf;
    rec_error = 0;

    dir = opendir(path);
    if(dir == NULL) {
        rec_error = 1;
        return 1;
    }
    
    while((entry = readdir(dir)) != NULL) {
        int err = 0;
        if(strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
            snprintf(fullPath, 512, "%s/%s", path, entry->d_name);
            printf("x.%s\n",fullPath);
            
            if(lstat(fullPath, &statbuf) == 0) {
                if(findAllParse(fullPath) == 0)
                    err = 1;
                if(S_ISDIR(statbuf.st_mode)) {
                    findAll(fullPath);
                }
            }
            if(!success_printed && !rec_error){
                    printf("SUCCESS\n");
                    success_printed = 1;
                }
            if(!err){
                printf("%s\n",fullPath);
            }

        }
    }
   // printf("%s\n",path);
    
    closedir(dir);
    return 0;
}

int errHandle(int err){
    if(err == 0)
        return 0;
    printf("ERROR\n");
    switch(err)
    {
        case(1): {
            printf("invalid directory path\n");
            break;
        }
        case(2): {
            printf("Bad command\n");
            break;
        }
        case(3): {
            printf("Bad file\n");
            break;
        }
        case(4): {
            printf("wrong magic");
            break;
        }
        case(5): {
            printf("wrong version");
            break;
        }
        case(6): {
            printf("wrong sect_nr");
            break;
        }
        case(7): {
            printf("wrong sect_types");
            break;
        }
        case(8): {
            printf("invalid file");
            break;
        }
        case(9): {
            printf("invalid section");
            break;
        }
        case(10):{
            printf("invalid line");
            break;
        }
        default: {
            printf("How did you even get here\n");
            break;
        }
    }
    printf("\n");
    return err;
}

int main(int argc, char **argv){
    int err = 0;
    char path_buffer[MAX_PATH+5];
    enum possible_modes{variant=0,list,parse,extract,findall};
    int file_handle = -1;
    int sect_nr = -1;
    int line_nr = -1;
    char flags = 0;
    int permissions = 0;
    int size_greater = 0;

    while(1)
    {
        if(argc >= 2)
        {
            enum possible_modes mode = 0;
            for(int i=1;i<argc;i++)
            {
                //printf("%s\n",argv[i]);
                if(strcmp(argv[i], "variant") == 0)
                {
                    mode = 0;
                }
                if(strcmp(argv[i],"list") == 0)
                {
                    mode = 1;
                }
                if(strcmp(argv[i],"parse") == 0)
                {
                    mode = 2;
                }
                if(strcmp(argv[i],"extract") == 0)
                {
                    mode = 3;
                }
                if(strcmp(argv[i],"findall") == 0)
                {
                    mode = 4;
                }
                if(strstr(argv[i],"recursive")!=NULL)
                {
                    flags = flags | 1;
                }
                if(strstr(argv[i],"size_greater=")!=NULL)
                {
                    flags = flags | 2;
                    sscanf(*(argv+i)+13,"%d",&size_greater);
                    
                }
                if(strstr(argv[i],"permissions=")!=NULL)
                {
                    flags = flags | 4;
                    char perm[10];
                    strncpy(perm,argv[i]+12,10);
                    perm[9]='\0';
                    //printf("---%s-----",perm);
                    for(int i=0;i<9;i++)
                    {
                        if(perm[i]-'-')
                            permissions=permissions<<1|1;
                        //maybe verificare pentru r w x
                        else
                            permissions<<=1;
                    }
                   // printf("%o",permissions);

                }
                if(strstr(argv[i],"section=")!=NULL)
                {
                    sscanf(argv[i] + 8,"%d",&sect_nr);
                }
                if(strstr(argv[i],"line=")!=NULL)
                {
                    sscanf(argv[i] + 5,"%d",&line_nr);
                }
                if(strstr(argv[i],"path=") != NULL)
                {
                    if(strlen(argv[i]) > MAX_PATH + 6)
                    {
                        err = 1;
                        break;
                    }
                   
                    strcpy(path_buffer,argv[i]+5);
                    
                    file_handle= open(path_buffer,O_RDONLY);

                    if(file_handle==-1)
                    {
                        err = 8;
                        break;
                    }
                }
            }
           
            switch(mode){
                case 0: {
                    err = variantPrint();
                    break;
                }
                case 1: {
                    // DIR *dir = NULL;
                    // dir = opendir(path_buffer);
                    // if(dir == NULL)
                    //                         err = 1;
                    //     closedir(dir);
                    //     break;
                    // }
                     printf("SUCCESS\n");
                    // closedir(dir);
                    err = listDir(path_buffer,flags,size_greater,permissions);
                    break;
                }
                case 2: {
                    err = parseFile(file_handle,0);
                    break;
                }
                case 3: {
                    err = extractLine(file_handle,sect_nr,line_nr);
                    break;
                }
                case 4: {
                    err = findAll(path_buffer);
                    break;
                }
            }
            break;
        }
    }
    if(file_handle!=-1)
        close(file_handle);
    return errHandle(err);
}